-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 16, 2019 at 07:53 AM
-- Server version: 10.2.25-MariaDB-log
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobileapitest_simplifyusers`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `user_type` varchar(50) NOT NULL,
  `client_url` varchar(255) NOT NULL,
  `service_url` varchar(255) NOT NULL,
  `status` smallint(1) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `mobile`, `user_type`, `client_url`, `service_url`, `status`, `date_created`) VALUES
(1, 'simplifyalert@gmail.com', '', 'client', 'https://simplifytest.info', 'https://service.simplifytest.info', 1, '2019-01-30 18:44:38'),
(2, 'sourabhgupta3838@gmail.com', '', 'client', 'https://demo1.simplifyvmsdemo.com', 'https://service.demo1.simplifyvmsdemo.com', 1, '2019-01-30 18:44:38'),
(3, 'archsureshgupta@gmail.com', '', 'client', 'https://demo1.simplifyvmsdemo.com', 'https://service.demo1.simplifyvmsdemo.com', 1, '2019-01-30 18:44:38'),
(4, 'sourabhg@ustechsolutions.com', '', 'client', 'https://demo1.simplifyvmsdemo.com', 'https://service.demo1.simplifyvmsdemo.com', 1, '2019-01-30 18:44:38'),
(5, 'simplifydemo@gmail.com', '', 'client', 'https://demo1.simplifyvmsdemo.com', 'https://service.demo1.simplifyvmsdemo.com', 1, '2019-01-30 18:44:38'),
(6, 'simplifydemoinfo@gmail.com', '', 'client', 'https://demo1.simplifyvms.info', 'https://service.demo1.simplifyvms.info', 1, '2019-01-30 18:44:38'),
(7, 'simplifydemoinfohr@gmail.com', '', 'client', 'https://demo1.simplifyvms.info', 'https://service.demo1.simplifyvms.info', 1, '2019-01-30 18:44:38'),
(8, 'simplifydemo123+8946@gmail.com', '', 'client', 'https://simplifytest.info', 'https://service.simplifytest.info', 1, '2019-01-30 18:44:38'),
(9, 'simplifydemo123+6894@gmail.com', '', 'client', 'https://simplifytest.info', 'https://service.simplifytest.info', 1, '2019-01-30 18:44:38'),
(10, 'simplifydemo123+7631@gmail.com', '', 'client', 'https://simplifytest.info', 'https://service.simplifytest.info', 1, '2019-01-30 18:44:38'),
(1201, 'simplifyalert+aly123@gmail.com', '', 'client', 'https://tuftsvms.net', 'https://service.tuftsvms.net/', 1, '2019-06-11 00:00:00'),
(1202, 'simplifytuftsadmin@gmail.com', '', 'client', 'https://tuftsvms.net/', 'https://service.tuftsvms.net/', 1, '2019-06-11 00:00:00'),
(1203, 'simplifyalertsall@gmail.com', '', 'client', 'https://salliemaevms.net', 'https://service.salliemaevms.net', 1, '2019-06-11 00:00:00'),
(1204, 'simplifymspalert@gmail.com', '', 'program', 'https://demo1.simplifyvms.info', 'https://service.demo1.simplifyvms.info', 1, '2019-06-21 00:00:00'),
(1205, 'simplifyalert+6460@gmail.com', '', 'client', 'https://demo1.simplifyvms.info', 'https://service.demo1.simplifyvms.info', 1, '2019-06-24 09:03:52'),
(1206, 'simplifyalert+2334@gmail.com', '', 'client', 'https://demo1.simplifyvms.info', 'https://service.demo1.simplifyvms.info', 1, '2019-06-26 13:44:12'),
(1207, 'simplifyalert+ja1@gmail.com', '', 'client', 'https://demo1.simplifyvms.info', 'https://service.demo1.simplifyvms.info', 1, '0000-00-00 00:00:00'),
(1208, 'simplifyalert+tm1@gmail.com', '', 'client', 'https://demo1.simplifyvms.info', 'https://service.demo1.simplifyvms.info', 1, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_2` (`email`,`user_type`,`client_url`),
  ADD KEY `email` (`email`),
  ADD KEY `mobile` (`mobile`),
  ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1210;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

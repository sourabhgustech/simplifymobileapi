<?php
// HIDE ON PRODUCTION
error_reporting(E_ALL);
ini_set('display_errors', 1);

include_once '../src/Epi.php';
include_once 'config.php';
Epi::setPath('base', '../src');
Epi::init('route','api','database');

EpiDatabase::employ('mysql',$config["database_name"],$config["database_host"],$config["database_username"],$config["database_password"]);
// type = mysql, database = mysql, host = localhost, user = root, password = [empty]



define('FAIL', 'failure');
define('SUCCESS', 'success');
define('SECRET', 'simplifyVMSaPP');
/*
 * We create 3 normal routes (think of these are user viewable pages).
 * We also create 2 api routes (this of these as data methods).
 *  The beauty of the api routes are they can be accessed natively from PHP
 *    or remotely via HTTP.
 *  When accessed over HTTP the response is json.
 *  When accessed natively it's a php array/string/boolean/etc.
 */
getRoute()->get('/', 'verify');
getRoute()->post('/verify', 'verify');
getRoute()->get('/gettoken', 'gettoken');
getRoute()->get('/import_users', 'import_users');
getRoute()->run();
/*
 * ******************************************************************************************
 * Define functions and classes which are executed by EpiCode based on the $_['routes'] array
 * ******************************************************************************************
 */

function verify() {
  try {
    $email = checkToken();
    $user = getDatabase()->all('SELECT * FROM users WHERE email=:EMAIL', array(':EMAIL' => $email));
    
    if(!empty($user)) {
      provideResponse($user, SUCCESS);
    } else {
      provideResponse("Email doesn't exists.", FAIL);
    }
  } catch(Exception $e) {
    json_response($e->getMessage(), 500);
  }
}

function gettoken() {
  try {
    $email = !empty($_GET["email"]) ? $_GET["email"] : "";
    if(!empty($email)) {
      $nonce = time();
      echo "<br>email: ".$email;
      echo "<br>nonce: ".$nonce;
      echo "<br>token: ".base64_encode(base64_encode(urlencode($email)).SECRET.base64_encode(urlencode($nonce)));
    }
  } catch(Exception $e) {
    json_response($e->getMessage(), 500);
  }
}

function checkToken() {
  try {
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
    $email = !empty($_POST['email']) ? $_POST['email'] : "";
    $nonce = !empty($_POST['nonce']) ? $_POST['nonce'] : "";
    $senttoken = $_SERVER['HTTP_X_TOKEN'];
    if(empty($email)) {
      provideResponse('Email is empty.', FAIL);
    }
    if(empty($nonce)) {
      provideResponse('Nonce is empty.', FAIL);
    }
    if(empty($senttoken)) {
      provideResponse('Token is empty.', FAIL);
    }
    if((time() - $nonce) >= 600) {      
      provideResponse('Token Expired.', FAIL);
    }

    $sentemail = extractToken($senttoken);
    $token = base64_encode(base64_encode(urlencode($email)).SECRET.base64_encode(urlencode($nonce)));

    //echo "token sent---".$_SERVER['HTTP_X_TOKEN'];
    if($senttoken == $token) {
      return $email;
    } else {
      provideResponse('Invalid Token', FAIL); 
    }
  } catch(Exception $e) {
    json_response($e->getMessage(), 500);
  }
}

function extractToken($senttoken) {
  $bcdecode = base64_decode($senttoken);
  $replaceSalt = explode(SECRET, $bcdecode);
  $sentemail = '';
  if(!empty($replaceSalt[0])) {
    $sentemail = base64_decode(urldecode($replaceSalt[0]));
  }
  return $sentemail;
}

function provideResponse($res, $status = SUCCESS) {  
  if(!is_array($res)) {
    $msg = $res;
    $res = array();
    if($status == FAIL) {
      $res[] = array("error" => $msg);
    } else {
      $res[] = array("success" => $msg);
    }
  }

  if($status == FAIL || $status == SUCCESS) {
    $code = 200;
  } else {
    $code = $status;
  }
  json_response($res, $code);
}

function json_response($message = null, $code = 200)
{
    // clear the old headers
    header_remove();
    // set the actual code
    http_response_code($code);
    // set the header to make sure cache is forced
    header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
    // treat this as json
    header('Content-Type: application/json');
    $status = array(
        200 => '200 OK',
        204 => 'No Content',
        203 => 'Non-Authoritative Information',
        400 => '400 Bad Request',
        422 => 'Unprocessable Entity',
        500 => '500 Internal Server Error'
        );

    // ok, validation error, or failure
    header('Status: '.$status[$code]);
    // return the encoded json
    echo json_encode(array(
        'status' => $code < 300, // success or not?
        'message' => $message
        ));
    exit;
}

function import_users() {
  $type = !empty($_GET['type']) ? $_GET['type'] : "client";
  $service = "http://lan.vms-client.com";
  $table = 'vms_client';
  $clienturl = "http://vms-client.com";
  if($type == 'program') {
    $table = 'admin';
  } elseif($type == 'vendor') {
    $table = 'vms_vendor';
  } elseif($type == 'candidate') {
    $table = 'vms_candidates';
  }

  $d = EpiDatabase::employ('mysql','simplifytestinfo_test_03dec','localhost','simplifytestinfo_test','OvwTcBP+&L2&');
  $user = getDatabase()->all('SELECT * FROM '.$table);
  if(!empty($user)) {
    $i = 0;
    foreach($user as $u) {
      EpiDatabase::employ('mysql','simplifyvms_users','localhost','mobileapitest_simplifyusers','$%Elk6j*z]ut');
      $user = getDatabase()->all('SELECT * FROM users WHERE email = "'.$u["email"].'" AND user_type = "'.$u["type"].'"');
      if(empty($user)) {
        $i++;
        $id = getDatabase()->execute("INSERT INTO users VALUES('','".$u["email"]."','','".$type."','".$clienturl."','".$service."','1',NOW())");
        if(!empty($id)) {
          echo "<br>".$i.". User . ".$u["email"]." from ".$type." is inserted successfully.";
        }
      }
    }
    echo "<br><strong>Total Users Imported:</strong> ".$i;
  }
} 

function dd($arr) {
  echo "<pre>";
  print_r($arr);
  echo "</pre>";
}